<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

use App\User;
use Illuminate\Http\Request;

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Route::get('facebook', function () {
//     return view('facebook');
// });

Route::get('activate-email/{user}', function (User $user) {

    
    // if (! $request->hasValidSignature()) {
    //     abort(401, 'This link is not valid.');
    // }

    // $request->user()->update([
    //     'is_active' => 1
    // ]);

    $user->update([
        'is_active' => 1
        ]);

    return view('home');
})->name('activate-email');

Route::get('activateEmail/{email}', 'HomeController@activateEmail');

Route::get('auth/facebook', 'Auth\FacebookController@redirectToFacebook');
Route::get('auth/facebook/callback', 'Auth\FacebookController@handleFacebookCallback');

Route::get('auth/google', 'Auth\FacebookController@redirectToGoogle');
Route::get('auth/google/callback', 'Auth\FacebookController@handleGoogleCallback');

Route::get('auth/github', 'Auth\FacebookController@redirectToGithub');
Route::get('auth/github/callback', 'Auth\FacebookController@handleGithubCallback');

// Route::get('login/facebook/', 'FacebookController@redirectToFacebook')->name('redirectToFacebook');
// Route::get('login/facebook/callback', 'FacebookController@handleFacebookCallback');

Route::get('/{slug?}', 'CategoriesController@categories')->name('index');
Route::post('/subcategories', 'CategoriesController@subcategories')->name('subcategories');
Route::post('/search', 'CategoriesController@search')->name('search');

Route::get('courses/{slug?}', 'CoursesController@courses')->name('courses');
Route::post('listCourses', 'CoursesController@listCourses')->name('listCourses');

Route::post('/dashboard', 'AdminController@dashboard')->name('dashboard');
Route::post('/approveCourse', 'AdminController@approveCourse')->name('approveCourse');
Route::post('/disapproveCourse', 'AdminController@disapproveCourse')->name('disapproveCourse');
Route::post('/deleteCourse', 'AdminController@deleteCourse')->name('deleteCourse');
Route::post('/addCourse', 'CoursesController@addCourse')->name('addCourse');
Route::post('/vote/{id}', 'CoursesController@vote')->name('vote');

Route::post('/filterCourses', 'CoursesController@filterCourses')->name('filterCourses');










