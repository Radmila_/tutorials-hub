@extends('layouts.header')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 subcategory-title">
            @php
            $subcategory = \App\Subcategory::where('slug', \Request('slug'))->first();
            @endphp
        </div>
    </div>


    <!-- <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="row justify-content-center courses">
                
            </div>
        </div>
    </div> -->

    <div class="row">
        <div class="col-md-3 panel panel-primary intro-panel filters">
            <div class="panel-header">
                <div class="row panel-heading">
                    <div class="col-md-6">
                        <h6>Filter Courses</h6>
                    </div>
                    <div class="col-md-6">
                        <a href id="clear-filters">Clear Filters</a>
                    </div>
                </div>
                <hr>
            </div>
            <div class="panel-body">
                <!-- probaj da go napravis dinamicno -->
                <h6>Type of course</h6>
                <div class="type-append">

                </div>
                <h6>Medium</h6>
                <div class="medium-append">

                </div>
                <h6>Level</h6>
                <div class="level-append">

                </div>
                <h6>Version</h6>
                <div class="version-append">

                </div>
                <h6>Language</h6>
                <div class="language-append">
                    <span class="filters-number">

                    </span>
                </div>
            </div>

        </div>
        <div class="col-md-8 offset-md-1 courses courses-main-content">
            <div class="top-tut-container">
                <p class="top-tut-title">Top {{$subcategory->title}} tutorials</p>

            </div>

            <div class="courses-test">

            </div>
        </div>
    </div>
</div>

<!-- <script>
$(document).ready(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
  function listCourses() {
        let slug = window.location.pathname
        console.log(slug);
        $.post('/listCourses', slug, function(data) {
            console.log(data);
            data.forEach(element => {
            console.log(element);

                console.log(element.subcategories[0].slug);
                element.subcategories.forEach(sub => {
                    if ('/courses/' + sub.slug == slug) {
                    // console.log(slug);
                    // console.log(element.subcategories.slug);
                    console.log('success');
                   
                        $('.courses').append(`
                        <div class="col-md-4">
                            <div class="logo w-100 rounded p-3 my-4">
                                <a class="d-flex align-items-center" href="${element.url}">
                                <h6 class="ml-3">${element.title}</h6><br>
                                </a>
                            </div>
                        </div>
                        `)
                } else if (slug == '/') {
                    // console.log(slug);

                    element.subcategories.forEach(sub => {
                        $('.courses').append(`
                        <div class="col-md-4">
                            <div class="logo w-100 rounded p-3 my-4">
                                <a class="d-flex align-items-center" href="${sub.url}">
                                    <h6 class="ml-3">${sub.title}</h6>
                                </a>
                            </div>
                        </div>
                        `)
                    })
                }

            })
            });
        })
    }
    listCourses();

}) -->

</script>

@endsection