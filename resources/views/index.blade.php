@extends('layouts.header')

@section('content')
<div class="container">
    <h1 class="text-center find-courses-title">Find the Best Programming Courses &amp; Tutorials</h1>
    <div class="row">
        <div class="search form-holder col-md-10 offset-md-1">
            <input type="text" id="search" class="form-control navbar-search-input" 
            placeholder="Search for the language you want to learn: Python, Javascript...">
            <img class="icon color-filter" src="https://hackr.io/assets/images/header-icons/search-header.svg" width="17" height="17">
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="row justify-content-center subcategories">

            </div>
        </div>
    </div>
</div>
@endsection