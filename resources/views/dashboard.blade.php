@extends('layouts.header')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">

            Welcome
            {{\Auth::user()->name}}<br>
            <!-- <h3>Courses to be Approved</h3>

            <table style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Course Title</th>
                        <th>Type</th>
                        <th>Media</th>
                        <th>Level</th>
                        <th>Version</th>
                        <th>Language</th>
                        <th>Status</th>
                        <th>Action</th>


                    </tr>
                </thead>
                <tbody class="unapproved-courses">

                </tbody>
            </table> -->


            <h3>Courses to be Approved</h3>
            <table style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Course Title</th>
                        <th>Type</th>
                        <th>Media</th>
                        <th>Level</th>
                        <th>Version</th>
                        <th>Language</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody class="unapproved-courses">

                </tbody>
            </table>
            <br>
            <h3>Already Approved Courses</h3>
            <table style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Course Title</th>
                        <th>Type</th>
                        <th>Media</th>
                        <th>Level</th>
                        <th>Version</th>
                        <th>Language</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody class="approved-courses">

                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
@endsection