<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta property="og:title" content="Tutorials Hub">
  <meta property="og:image" content="http://www.tutorialshub.mk/images/web_thumbnail1.png">
  <meta property="og:image:secure_url" content="https://www.tutorialshub.mk/images/tutoJPG.jpg">
  <meta property="og:description" content="Tutorials Hub">
  <meta property="og:url" content="http://www.tutorialshub.mk/">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('header.name', 'Tutorials Hub') }}</title>

  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}" defer></script>
  <script src="{{ asset('js/main.js') }}" defer></script>



  <!-- Fonts -->
  <link rel="dns-prefetch" href="//fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <link href="{{ asset('css/style.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">


  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>


  @include('layouts.scripts')

</head>

<body>
  @if (\Auth::user() && \Auth::user()->is_active == 0)
  <p class="text-center confirm-email">Please confirm your email address!</p>
  @endif
  <header>
    <nav class="navbar navbar-expand-lg navbar-light">
      <a class="navbar-brand" href="/">
        <img class="brainster-logo" src="https://brainster.co/static/Logo-large.svg" alt="Brainster Logo">
        <h6 class="text-center logo-text">Brainster <br>Tutorials Hub</h6>
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          @php
          $categories = \App\Category::all();
          @endphp
          @foreach($categories as $category)
          <li class="nav-item">
            <a class="nav-link" href="/{{$category->slug}}">
              <span><img class="nav-icon " src="{{$category->icon}}" alt="Data Science"></span>
              {{ $category->title }}</a>
          </li>
          @endforeach

        </ul>
        <!-- Right Side Of Navbar -->
        <ul class="navbar-nav ml-auto">

          @if (\Auth::user() && \Auth::user()->is_admin == 1)
          <li class="nav-item">
            <button type="button" class="btn btn-primary right-nav-btn"><a href="{{ route('dashboard') }}" class="admin-btn">Admin dashboard</a></button>
          </li>
          @endif


          <li class="nav-item">
            <button type="button" class="btn btn-primary right-nav-btn" data-toggle="modal" data-target="#exampleModal" data-whatever="" href="">Submit a tutorial</button>
          </li>
          @if (\Auth::user())
          @if(\Auth::user()->is_active == 0)
          <!-- Please, confirm your email address in order to submit a tutorial -->
          <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title-log-in" id="exampleModalLabel">Please Confirm Your Email Address</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <h4 class="modal-message">Please, confirm your email address in order to submit a tutorial</h4>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
          @else
         
          <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">New Tutorial</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">

                  <form method="POST" action="{{ route('addCourse') }}">
                    @csrf
                    @php
                    $user = \Auth::user();
                    @endphp

                    <input type="hidden" name="user_id" id="user_id" value="{{$user->id}}">
                    <div class="form-group">
                      <label for="title" class="col-form-label">Course Title:</label>
                      <input type="title" class="form-control" id="title" name="title">
                    </div>
                    <div class="form-group">
                      <label for="url" class="col-form-label">Course URL:</label>
                      <textarea class="form-control" id="url"></textarea>
                    </div>
                    <!-- //dropdown -->
                    <div class="form-group">
                      <label for="subcategory_id">Subcategory</label><br>

                      <select name="subcategory_id" class="form-control subcategory @error('subcategory') is-invalid @enderror">
                        @php
                        $subcategories = \App\Subcategory::all();
                        @endphp
                        @foreach($subcategories as $subcategory)
                        <option value="{{ $subcategory->id }}">{{ $subcategory->title }}</option>
                        @endforeach
                      </select>
                    </div>

                    <div class="form-group">
                      <label for="version_id">Version</label><br>

                      <select name="version_id" class="form-control version @error('version') is-invalid @enderror">
                        @php
                        $versions = \App\Version::all();

                        @endphp
                        @foreach($versions as $version)
                        <option value="{{ $version->id }}">{{ $version->version }}</option>
                        @endforeach
                      </select>
                    </div>

                    <div class="form-group">
                      <label for="type_id">Type</label><br>

                      <select name="type_id" class="form-control  type @error('type') is-invalid @enderror">
                        @php
                        $types = \App\Type::all();
                        @endphp
                        @foreach($types as $type)
                        <option value="{{ $type->id }}">{{ $type->type }}</option>
                        @endforeach
                      </select>
                    </div>

                    <div class="form-group">
                      <label for="media_id">Choose a medium</label><br>

                      <select name="media_id" class="form-control medium @error('media') is-invalid @enderror">
                        @php
                        $media = \App\Medium::all();
                        @endphp
                        @foreach($media as $medium)
                        <option value="{{ $medium->id }}">{{ $medium->medium }}</option>
                        @endforeach
                      </select>
                    </div>

                    <div class="form-group">
                      <label for="level_id">Choose a level</label><br>

                      <select name="level_id" class="form-control level @error('level') is-invalid @enderror">
                        @php
                        $levels = \App\Level::all();
                        @endphp
                        @foreach($levels as $level)
                        <option value="{{ $level->id }}">{{ $level->level }}</option>
                        @endforeach
                      </select>
                    </div>

                    <div class="form-group">
                      <label for="language_id">Choose a language</label><br>

                      <select name="language_id" class="form-control language @error('language') is-invalid @enderror">
                        @php
                        $languages = \App\Language::all();
                        @endphp
                        @foreach($languages as $language)
                        <option value="{{ $language->id }}">{{ $language->language }}</option>
                        @endforeach
                      </select>
                    </div>

                  </form>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-primary btn-submit-tutorial" data-toggle="modal" data-dismiss="modal" data-target="#submitMessageModal" data-whatever="" href="">Submit tutorial</button>
                </div>
              </div>
            </div>
          </div>
          @endif
          @else
          <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title-log-in" id="exampleModalLabel">Please Log In</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <h4 class="modal-message">Please, log in to submit a tutorial</h4>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
          @endif

          <!-- Successfully submited tutorial modal -->
          <div class="modal fade" id="submitMessageModal" tabindex="-1" role="dialog" aria-labelledby="submitMessageModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="submitMessageModalLabel">Thank you for your contribution!</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <p class="modal-message">Your tutorial has been submited successfully. You will be able to see the tutorial once the admin has approved it. Thank you for your contribution!</p>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>

          <!-- Please log in to vote modal -->
          <div class="modal fade" id="voteModal" tabindex="-1" role="dialog" aria-labelledby="voteModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title-log-in" id="voteModalLabel">Please Log In</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <h4 class="modal-message">Please, log in to vote for this tutorial</h4>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>

          <!-- Please, confirm your email in order to vote -->
          <div class="modal fade" id="confirmEmailModal" tabindex="-1" role="dialog" aria-labelledby="confirmEmailModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title-log-in" id="confirmEmailModalLabel">Please Confirm Your Email Address</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <h4 class="modal-message">Please, confirm your email address in order to vote for this tutorial</h4>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>



          <!-- Authentication Links -->
          @guest
          <li class="nav-item">
            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
          </li>
          @if (Route::has('register'))
          <li class="nav-item">
            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
          </li>
          @endif
          @else
          <li class="nav-item dropdown">
            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
              {{ Auth::user()->name }} <span class="caret"></span>
            </a>

            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
              </a>

              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
              </form>
            </div>
          </li>
          @endguest
        </ul>

      </div>
    </nav>
</header>

    <main class="py-4">
      @yield('content')
    </main>

    <footer class="footer text-center">
    <h6>Made with
      <span style="font-size:150%; color:red;">♥</span>
      by Radmila Serafimoska
    </h6>
  </footer>

  </body>

</html>