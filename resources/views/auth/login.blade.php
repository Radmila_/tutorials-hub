@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" placeholder="Email" type="email" class="form-control @error('email') is-invalid @enderror" style="padding-left:37px" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                <img class='email-icon' src='https://img.favpng.com/3/3/18/email-computer-icons-mobile-phones-sms-clip-art-png-favpng-JCCHCL7zXzbKQwK9zNJ3mu90g.jpg'>

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" placeholder="Password" type="password" class="form-control @error('password') is-invalid @enderror" style="padding-left:37px" name="password" required autocomplete="current-password">
                                <img class='password-icon' src='https://icon2.cleanpng.com/20181128/itt/kisspng-computer-icons-portable-network-graphics-scalable-modify-login-password-svg-png-icon-free-download-5bfe97eec27159.2169320615434116947965.jpg'>

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                                @endif
                            </div>
                        </div>

                    </form>
                    <br>
                    <div class="social-buttons">
                        <div class="gmail-login btn-gmail col-md-6 offset-md-4">
                            <a href="{{ url('auth/google') }}" class="btn btn-gmail">
                                <strong>
                                    <img class="social-logo" src="https://image.flaticon.com/icons/svg/732/732200.svg" alt="gmail Logo">
                                    Login With GMail</strong>
                            </a>
                        </div>
                        <div class="gitlab-login btn-github col-md-6 offset-md-4">
                            <a href="{{ url('auth/github') }}" class="btn btn-gitlab">
                                <strong>
                                    <img class="social-logo" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAjVBMVEUmJib///8AAAAjIyMaGhofHx8VFRUPDw8YGBgUFBQgICAcHBwLCwsICAjU1NTx8fHl5eXo6OiXl5fu7u75+fmHh4e/v7+jo6Pb29uBgYHHx8dAQEC3t7d6enpcXFzR0dEzMzOtra1HR0cxMTFVVVVjY2NsbGydnZ2SkpJ0dHRnZ2cyMjI6OjpMTExWVlaNxirZAAAITElEQVR4nO2dWZeiOhCASYV9EVtRW8UFl1b72v3/f94FexmRFAjGQJ9T38s8zB2pIqH25GoaQRAEQRAEQRAEQRAEQRAEQRAEQRAEQRAEQRAEQRAEQRBEbVzPsQIAsBzbsdI/A8vx3LaFkoWrpyrNF4N41usPwzAKw2G/N4sHi3mqsP7n1dQDmE/jVybmNZnOIfDaFrIxXAdYzEJEux/C8QnA420L2wAjsE+zCu1+mJ1M32hb4JrosIujO/XLiOIP0NsWugYmTPs11PvidQF224LfiQ2jqo9PzHD6J9bRgMWwkX4XHU/Q9e+Rw2f9/XnN6wd02q6axvgh/TJis7ufI4fFw/qlRKeuLqPn3Ov/qhhDJ+OcYFnHAZYTfvhtq1MEBtL0yxhB2wrdwOFxE5Mn7tbHaHhY+tCcldWhzEo/N3fyOC9aZ+yNvpFnY64J1x1R8VkKpp7x3AkVvfOzFExV3HYgTHV5s0TiPoZu6+aGWy9PVJCxfutOA3pPVZCxWcuuH+InK8jYpFUVrenTFWTszWxPQWOjQEHG1u1ZGyhamYW+3Y3GzfxHGE93W724LV5b26eQFISJgHPDDmBfPxAfLyGwDc6h+HoGLeVS3lIgZvD9d+BO6qgXHfSfGlQgeDmbdhw/CMLtxW85kPs8uVvBiWf9uj3BNk29YhsK+qJFOl/5Zx5o18sRDXvjcZxMkng87g2vt2Ls+lf/zJ0LfnfkqFeQrwWChPl3zWF5WedeMlqubYDA9y3L8v0AwF4vR8klWnjZ3YQtgg+RRbr60AZEZafV7W7yYDJZAlipCbn5m9QgWQDLyaRQAwZRNp0o36fGh0CMX0NzheOUWQnDKW6/QFiyO6t2irASiZFYEn7aF0aCY8WLKF5CNpFhEKxE+NtrtV+ieAnZRMYaIhrGxS/giQgteiaFjOBD5PIzNJWL6Itfs5xkDtkfbKDSJ4JYBvYiRUOkLBIptDU2mha6j+8kYShx4V1d5U3oky+8PS6EKC79Ql1Bg58xGdjscVMTIJ9hSiEwehbmEZVBwhp6b+ivn1QNMuCbdCTDZwUjdIcoconcwyQoBN7NwNyFMmuqo916ST6Za9gDlmpyfXFknDKQEbNlWAfkCVLi3moEFbYLQ3l7CHP6PSXblG+RF3yU94IdZCggUlIcRq25REPHTeQZGxV5sIm8X0F+3xxhjSRlpGJeykeSGwnO/h+YvR6rqA1j/TSp6Rv2sSsp8CN2TnLRVlRuZmp8PteR/SM3ohIX3JQk+i7SUZPsjJFiDft4vjE19uJHT+XG/TYSfivIgr2T+NELuRpiebDkx4jAXu5C7svF3IUCh4ilv7LXENFQQcHNQTqfinaplJpzOZiGkrcP9jFI6YyUYyEaHuWG/Vj0q0BDbA0lPxrzhwp2KZpayI2JsebF4fmWBvtAJKffWD1PgbfAzHgoWUOkjqHA43vv4kezrdTsCSurK4ja3J2KZ6Olkt3zI2+0mCnVymGmVEmbFOsdSk2/ATnjp6TqjT2ccXmvF61YKqliYNm3zKgGq5fKriQgT8dK7hLfL7pPlPTyUXfBPmX1TfBH7FU0uvHOkKTmWtkBADUTJ1hnhrGlnDfsIaUgZXOmaHdNlgD4K5QykVQN3iFlBynzNAn6+yc18ybcQCVgy8dDfwefVGCOomEMfFKBsYfP0+nIyFyGmgaphifBGY8eGTTLDjMq6a1lYJOJXyrOH4nAAyxzuaBuxLRsm2YHzZsK4pYfeZfmb6vBKhnfzHijZeTBufyon4L8/hcsg/ph4Ft115H7XlL+oyqnL2+cfi+e3UbK4UGrc7cFt+G/SdVxMAWl0n/kmohL8ANYD24FnJ0sMO+YJuSeA+4UH0j85ax0lP169OyY7h6+SYxiMLca7FzwHR3LObzsUIm2P9x10lbxcQTj8+rZqfHk85cTCJsp/Xiw2ItVNE6jZHX3bQxKRmmuyCU4q63OwXnbYAHrQJz9m1guLUL5iWcjd/gwWjpa0GOIO4sx4eqck1a9hIUh0L3NtwdDOBmKW3nReVEE1YeCtGLodja4afwnGq4d4RUqB5+nvkHpcZJvgiQnwkugGW9sWrQ2UUnKgw3nFJA2uloH7uebJxOfa+Gs+GmV7i9sQu+Gdk7JavpNqrrl3HTdgszTspSnIsL9YdfSFSc3d0Nli8XnsZbkpZuXGUFswiqP+gOk33Arv0+3POsZhbC/Sq6iuLwSbt2hoJTTVM24OZGfdZ+sw9jWYZ6swigKe8keKqTDOqHXtHQa/0u+XFQSZamvmeZN/d4avjiON+VmHq8c/jJtw47+EzDn4r+/F6PPjqb9yg7WpnIaDO1Q/IJGRGrgfk7C5aVxwq2zrtnj4VT3Pj8qGlKVGrZ37cc3rpYLvHZfOyrbmX6WAlde81Sl4dBu+xYlzctHb3Vv5qzQsBOXfdmfOZli26+TBZRr2JEL25ybW1ySOfi253LuZjl8RUuzVMNo3gkFUx942w17iUf7zXm+e5smvV55X7pMw2jTmauhnQ88z6sImks0HG47soIZ+hwNTao0RD1+n3fAyPzDcLC1aKrhrPXL9m7g2LhkQw2l9FrlwkGc6lUkBmINo/fuKZhizUU7tUpDUb20p3X00nIDBAXQBhoeu/YJXhFsCstYW8PVttVsqQqj8DVWnH++zYCH7x1ewC8cM2muYTjq5n3zebivJc00jI5+ixd51oH7/uFX8Lujtv4U/oh+GdzyF98Ft4qp05+DYrPln/m/zPygw/nwGg6PVa4bBsOwf+R/4fsr4Drg3LHxnCD9r7puPwmCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiC6Az/A7wlbzvM/tZvAAAAAElFTkSuQmCC" alt="gmail Logo">
                                    Login With GitHub</strong>
                            </a>
                        </div>
                        <div class="facebook-login btn-facebook col-md-6 offset-md-4">
                            <a href="{{ url('auth/facebook') }}" class="btn btn-facebook">
                                <strong>
                                    <img class="social-logo" src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTx5beFXvWOVbaP5gyvCcpziUlhgAxA82OACw&usqp=CAU" alt="gmail Logo">
                                    Login With Facebook</strong>
                            </a>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection