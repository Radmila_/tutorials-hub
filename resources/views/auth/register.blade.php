@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" placeholder="Full name" type="name" class="form-control @error('name') is-invalid @enderror" style="padding-left:37px" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                <img class='full-name-icon' src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAM8AAADzCAMAAAAW57K7AAAAjVBMVEX///8AAAAUFBSFhYXt7e35+fnw8PAMDAz4+PjFxcXi4uLq6ur8/PxDQ0PX19dqamrNzc1zc3OMjIw5OTmdnZ2qqqpPT0+/v7+wsLB+fn6VlZW4uLimpqbl5eUwMDDU1NR4eHhWVlYiIiI8PDwsLCwgICCIiIiampoZGRlra2tcXFxKSkpTU1NiYmIRERFMbR1WAAALTElEQVR4nO2da3uyPAyA51QEPAyPIJ6nTqfT///zXpnbTNpyEJIWnuu9Pz6HtLU0SdM0fXnhw/XHH/3T8P1aq9Va++VXfzqa9SzGBvlYzd+iUSjZhI5tun/P4M6/4obyx3Lgme5mNtzBe+pgfug7pjubRnM8zDqYO3XXdJcTcNfPDeabs2+62zH0NjlGE3EMTHddQS9dBcTTKtuIGm8FRhPxXirVEBYcTcRX2/QoflkdCYZz42B6IHemWfrayvKPliVQ3r19Ugf7B3/VtuzO7R82bct1gsEpcTLnpoczj+3aZd7rKP+L5Qzije5J/X90EaPWXuur5P9nzU4xA3o1+M3Z6t+5nzKYn/8cbNUjMuYvuKpF/jrKvhXofSoHZGgRrRRd2c+ek9FQKscBT4eT8RQdyeG2WH2FnCl9d9NwyH5W9yyL6tN2Nh15ds6L3MICeUBrsp5moid1oNAitmTnXOsaWoitv+efnDuyXdao5ZqvQttvxWXKM65vByEu4BGF0KZknIvOeVZEm0H1Q14EuXsiuSn4QrOZvJtMiOGUC5nkBBpCo5T+Y12QrSOs0OUbjvwp8+/BR7jBHrF4wfvpEouXaOP26H17wbJyWyGsqkkUtYAQ/eY9XcG6jcCMygj6hqWNP1BT7zxtCJ47nTmQOaCWuHb6WMkxqoSmpqWKlxCfHxdq+t2wbzrkagZPD6e3iP0ErglCprTO1Mg3HTSeM1MrqBHeOCbegVM7IXeQ7eG22zvYGE90BLoGrywtALDh5shWQJ7bmKEBDJogjn0DUtb8pwBoBXHYBnjOEzLIF0EfHP0+yOUVLzPgVT/wc9uQS1eAYnz0Jgge1jx5iJATtLGn1nA2FN4kFq4GhUypN8LQHmgJIwkGgtq9gh6iruSUJWiT2smGy0dXIgfScMTfOJCsKQwrbLxpt93Q+mg7O0M6iNbDmhlYPngB0R6pQmvKGXDBwOP8L1LJMA9EX6ox3BC3SCUDW30lFZwIUgikkoHcLangRFCch9RKmFBvgoKjDCJAwayBHQwKkFGm00NXSsde7hd4A4LSqYfmVGf2EwwiUJo9l0luGtCgUjoIUNHo2czdgSkJlN9FGcZDOT8uk9w04PfGtX5M6QPK7wLqa52JXKBZUvsDzy805kKy+Qfwh9IUDYmw4HhIz89AwtuSUm4yKKmTVPIXl+BEZmzjgWk1DVLJScAAz4RUMtwp6suD3IBW16SS4U6RI2lHDbxaQ2v2YLD/RCo5ARTwJb5NzLYyE0DqgDgKAz9lnuNzGaiEqK0EDMDpurcHr+NQeyXQtGmK8KDoDnkiJBSu54ABHS+QBzHhAtKjseGlAvoD+zGQviOXrgA5b/S/IErr1BGSR1cnGZLT4HEzb5LqN2ivwJHSBz84DRqBOZ1C2Cyyb1JxRh9LEvYbewsAlP3E83kjhbNmaeIP9DFwqR+UksZ7eQqlYHPt8JG/S7tfFEDJXHwRWdQK57VxlIp0ZGsGqWzGrCTcDmPAHF2oZzOq+IYJp3M10/LD4boIrOGXJWqKR8d9oDZY9Y5Q84AlM0m4DspcfgPf12MI9WBVzX6agQ03vR9nYfl8uvoXrBKoT+87wm1ADRstoUQN6c6xg/WNlrOzTottQOJw2G5mIcRKBWQ/oi2WjtJ0kiHWkiDKUBI0G3nIOh6xDNCZwpWTCsZoLJ/WFdsuHtIeiCJ1lqyxpaJnBbWCJdXg4fVzRMSiDrdvrkhEYSaJ05gF+Y1UsabAFDU2kqyl9jJwkja6dSKfQlIUk1zqucGCUMxQ7fS8N6yo9VQbGinS11DVgz49p+nGYmWiCL2q4IGtLLDXzRwoaavLlq75epyGWAHoh3qGSWrOYgrk6ksHUDBS96nWqjtJa6AdxJWD1OfkqFFV6fvhPHAUAYZmL1jHlyAdGi8ubyfXVV6+fcwD3/FWnjMLDutNcp1YI4UTRcaJXXyCo67EhhSsDclwSjE5dxTFIZ9lUppy0d/EKbqMlKucd0RH2sBk56gzRz0zzZxVypelHM03wTK9+wKXcj+T0ctUrfyX3ci4/UzHyfiqxDIsQXn1bPQOk5TB9INy6ed0esFUCgNFvF5Cv2pj+cNyndloMF3fmNbD8cxrG9hJF6TzIOWvY/+NcTquMw8/v7aZn/5BHIfny8fBX5Xi0SbbG709b3ZimNRnJpWe7deffL8oC6e5kY2DO1IUSiei1ff1Ko7FYZfeq2JcZrp0RSfgmxnEWseHtxCrIHMy5L6OvIqJufERMqpxT9OHhvlgGlEvzctkY8CgGgq/+VUI8nWUZTc9nFz60/pzTNdvm3MGP2lHmiviqQ42HpzrY69Y9pjd8w+XZJvWJzOxHfVDPT/NBHQul+2FSWuUKGc1Puh+HtG7jx0nPgZBkuMZJ747ZotmrOLabBVushGzGRgwP8fhqz+8osnl6tC0lhjgQjlJxdaQUkt3dcUAbUUQuVAejMqEbnVGNG3J+S1QOK2jCDhddT8PZwm/aX4VZyustok3MHvIzOauLyPnHNUm+mofIODpUl4ZbXk45l7FbfylbuTdscrD6Ro9C7hPUe4jY0sajunjWjv4CHOfSsoJiKU74HwKMUq4N6QIiBCza7hedNCEaJM1XG/mRMxONfB6JyViZqhpxVaQzvGfGo7oUlf8YxPzbSuuCkQntOKK+uUFb9r35TytzY7wtVU2T+AH4fZitX22F/F26Yfp7mSiY8UuCnwzTveVlVz0ouO1r5gIDc7fr8Li+Q2nKY0+vjmts65tXh6epirqhBydSlieR48VF6Oxrtb1WHQR4HqXYzXoxKoSXiiMrUsvyOFdj4nuPQ06lxJPCNgffKMHBaGEgxQ0Vi0F3ghA9h8bIRQC0R1zzwvaDVxi/6Yq0yMchcL4LTq3Km8CvgiaBnj2Ae9N0T5NwwtcQcCmMhdf5AMFox7BepSiX4E7BQ9gwtfjqRg4nGqFQJQ1zNHnVu4LLBKw678pSzBgXSVtEAFV9q/XCXcKGt8rIgGW1PrxedDZor7n2IiApuauytDOx3Dvngfq5rsnAI2SzrIqNMBd0D3aXpNGWCVg0PDb80TbiAoelMKsnCizGU7Yu+nO5QAmYUXG81Dp5YPnI6rEDE+wqhB1E2kI8wHLlFXO+kQACxSVjIPqoBRX2J4FHlrhwkD89Rc5gO5nAzlAlYjySsCKHz20g6jmaTZUcD4anc7HM+nAz0tCc1Q9byfCQjMCN0TVPDGFHtwUedeVND/Cc8DQPahMhQUM2F9/orflKuhdR4BE8Q2KYFV0POAKzARtHyoVSnwA0l27/4+nfPw/nnLzT48H7oaqkEWhAOywJ8g/qL7/9ob80ervf6a4fHolE0ahxzbCN0wrkbcjgAbgCFeXKnY69yKmWFviozElKQKamQbKC4sC8kJtymrkIv0i3MeOAiDiXblTFTJH71h9oe/fGx7xD2vTaoyoLV2GvucayJcZaye/7Jq76W/kbv/Mw0H+m9tYS1zi0B0rC2j/WRtlpcNa7fg58krmonbaThhXDPzxHo2dUJe6te2HM29hm/0Am5brBIPPxMp54KdX3NaWeB+e1vVwFPi+4/UWDeuGbTdpSyB2Os2mbUei2+7Kc/zZ+DCYfk6W8SXNH6BgWzu5wE4KrW+ur3m53gUU6YIYO2waK05FwlZe5wVrWBtF6Ue3N6a7lZNuXJzai1HcpWaXlF6tv/5eQSZpMQJrzlBTlIl9mMmHac+TX4koB9unCmh74cZ0hxM4D5wcqRIL50BXm5eI1mlQsMbvYjUbrS+mF9Xu1A/HHukBor1wV/4smIeD6brfv2wm3e1wudsfb/7Ktai3cnOZrtfr8XX/vltuu+evy2d/Pf0I50Hge+7iCSf/P+dpkjbCipUqAAAAAElFTkSuQmCC'>
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" placeholder="Email" type="email" class="form-control @error('email') is-invalid @enderror" style="padding-left:37px" name="email" value="{{ old('email') }}" required autocomplete="email">
                                <img class='email-icon' src='https://img.favpng.com/3/3/18/email-computer-icons-mobile-phones-sms-clip-art-png-favpng-JCCHCL7zXzbKQwK9zNJ3mu90g.jpg'>

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" placeholder="Password" type="password" class="form-control @error('password') is-invalid @enderror" style="padding-left:37px" name="password" required autocomplete="new-password">
                                <img class='password-icon' src='https://icon2.cleanpng.com/20181128/itt/kisspng-computer-icons-portable-network-graphics-scalable-modify-login-password-svg-png-icon-free-download-5bfe97eec27159.2169320615434116947965.jpg'>

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" placeholder="Confirm your password" type="password" class="form-control" style="padding-left:37px" name="password_confirmation" required autocomplete="new-password">
                                <img class='password-icon' src='https://icon2.cleanpng.com/20181128/itt/kisspng-computer-icons-portable-network-graphics-scalable-modify-login-password-svg-png-icon-free-download-5bfe97eec27159.2169320615434116947965.jpg'>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                    <br>
                    <div class="social-buttons">

                        <div class="gmail-login btn-gmail col-md-6 offset-md-4">
                            <a href="{{ url('auth/google') }}" class="btn btn-gmail">
                                <strong>
                                    <img class="social-logo" src="https://image.flaticon.com/icons/svg/732/732200.svg" alt="gmail Logo">
                                    Register With GMail</strong>
                            </a>
                        </div>
                        <div class="gitlab-login btn-github col-md-6 offset-md-4">
                            <a href="{{ url('auth/github') }}" class="btn btn-gitlab">
                                <strong>
                                    <img class="social-logo" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAjVBMVEUmJib///8AAAAjIyMaGhofHx8VFRUPDw8YGBgUFBQgICAcHBwLCwsICAjU1NTx8fHl5eXo6OiXl5fu7u75+fmHh4e/v7+jo6Pb29uBgYHHx8dAQEC3t7d6enpcXFzR0dEzMzOtra1HR0cxMTFVVVVjY2NsbGydnZ2SkpJ0dHRnZ2cyMjI6OjpMTExWVlaNxirZAAAITElEQVR4nO2dWZeiOhCASYV9EVtRW8UFl1b72v3/f94FexmRFAjGQJ9T38s8zB2pIqH25GoaQRAEQRAEQRAEQRAEQRAEQRAEQRAEQRAEQRAEQRAEQRBEbVzPsQIAsBzbsdI/A8vx3LaFkoWrpyrNF4N41usPwzAKw2G/N4sHi3mqsP7n1dQDmE/jVybmNZnOIfDaFrIxXAdYzEJEux/C8QnA420L2wAjsE+zCu1+mJ1M32hb4JrosIujO/XLiOIP0NsWugYmTPs11PvidQF224LfiQ2jqo9PzHD6J9bRgMWwkX4XHU/Q9e+Rw2f9/XnN6wd02q6axvgh/TJis7ufI4fFw/qlRKeuLqPn3Ov/qhhDJ+OcYFnHAZYTfvhtq1MEBtL0yxhB2wrdwOFxE5Mn7tbHaHhY+tCcldWhzEo/N3fyOC9aZ+yNvpFnY64J1x1R8VkKpp7x3AkVvfOzFExV3HYgTHV5s0TiPoZu6+aGWy9PVJCxfutOA3pPVZCxWcuuH+InK8jYpFUVrenTFWTszWxPQWOjQEHG1u1ZGyhamYW+3Y3GzfxHGE93W724LV5b26eQFISJgHPDDmBfPxAfLyGwDc6h+HoGLeVS3lIgZvD9d+BO6qgXHfSfGlQgeDmbdhw/CMLtxW85kPs8uVvBiWf9uj3BNk29YhsK+qJFOl/5Zx5o18sRDXvjcZxMkng87g2vt2Ls+lf/zJ0LfnfkqFeQrwWChPl3zWF5WedeMlqubYDA9y3L8v0AwF4vR8klWnjZ3YQtgg+RRbr60AZEZafV7W7yYDJZAlipCbn5m9QgWQDLyaRQAwZRNp0o36fGh0CMX0NzheOUWQnDKW6/QFiyO6t2irASiZFYEn7aF0aCY8WLKF5CNpFhEKxE+NtrtV+ieAnZRMYaIhrGxS/giQgteiaFjOBD5PIzNJWL6Itfs5xkDtkfbKDSJ4JYBvYiRUOkLBIptDU2mha6j+8kYShx4V1d5U3oky+8PS6EKC79Ql1Bg58xGdjscVMTIJ9hSiEwehbmEZVBwhp6b+ivn1QNMuCbdCTDZwUjdIcoconcwyQoBN7NwNyFMmuqo916ST6Za9gDlmpyfXFknDKQEbNlWAfkCVLi3moEFbYLQ3l7CHP6PSXblG+RF3yU94IdZCggUlIcRq25REPHTeQZGxV5sIm8X0F+3xxhjSRlpGJeykeSGwnO/h+YvR6rqA1j/TSp6Rv2sSsp8CN2TnLRVlRuZmp8PteR/SM3ohIX3JQk+i7SUZPsjJFiDft4vjE19uJHT+XG/TYSfivIgr2T+NELuRpiebDkx4jAXu5C7svF3IUCh4ilv7LXENFQQcHNQTqfinaplJpzOZiGkrcP9jFI6YyUYyEaHuWG/Vj0q0BDbA0lPxrzhwp2KZpayI2JsebF4fmWBvtAJKffWD1PgbfAzHgoWUOkjqHA43vv4kezrdTsCSurK4ja3J2KZ6Olkt3zI2+0mCnVymGmVEmbFOsdSk2/ATnjp6TqjT2ccXmvF61YKqliYNm3zKgGq5fKriQgT8dK7hLfL7pPlPTyUXfBPmX1TfBH7FU0uvHOkKTmWtkBADUTJ1hnhrGlnDfsIaUgZXOmaHdNlgD4K5QykVQN3iFlBynzNAn6+yc18ybcQCVgy8dDfwefVGCOomEMfFKBsYfP0+nIyFyGmgaphifBGY8eGTTLDjMq6a1lYJOJXyrOH4nAAyxzuaBuxLRsm2YHzZsK4pYfeZfmb6vBKhnfzHijZeTBufyon4L8/hcsg/ph4Ft115H7XlL+oyqnL2+cfi+e3UbK4UGrc7cFt+G/SdVxMAWl0n/kmohL8ANYD24FnJ0sMO+YJuSeA+4UH0j85ax0lP169OyY7h6+SYxiMLca7FzwHR3LObzsUIm2P9x10lbxcQTj8+rZqfHk85cTCJsp/Xiw2ItVNE6jZHX3bQxKRmmuyCU4q63OwXnbYAHrQJz9m1guLUL5iWcjd/gwWjpa0GOIO4sx4eqck1a9hIUh0L3NtwdDOBmKW3nReVEE1YeCtGLodja4afwnGq4d4RUqB5+nvkHpcZJvgiQnwkugGW9sWrQ2UUnKgw3nFJA2uloH7uebJxOfa+Gs+GmV7i9sQu+Gdk7JavpNqrrl3HTdgszTspSnIsL9YdfSFSc3d0Nli8XnsZbkpZuXGUFswiqP+gOk33Arv0+3POsZhbC/Sq6iuLwSbt2hoJTTVM24OZGfdZ+sw9jWYZ6swigKe8keKqTDOqHXtHQa/0u+XFQSZamvmeZN/d4avjiON+VmHq8c/jJtw47+EzDn4r+/F6PPjqb9yg7WpnIaDO1Q/IJGRGrgfk7C5aVxwq2zrtnj4VT3Pj8qGlKVGrZ37cc3rpYLvHZfOyrbmX6WAlde81Sl4dBu+xYlzctHb3Vv5qzQsBOXfdmfOZli26+TBZRr2JEL25ybW1ySOfi253LuZjl8RUuzVMNo3gkFUx942w17iUf7zXm+e5smvV55X7pMw2jTmauhnQ88z6sImks0HG47soIZ+hwNTao0RD1+n3fAyPzDcLC1aKrhrPXL9m7g2LhkQw2l9FrlwkGc6lUkBmINo/fuKZhizUU7tUpDUb20p3X00nIDBAXQBhoeu/YJXhFsCstYW8PVttVsqQqj8DVWnH++zYCH7x1ewC8cM2muYTjq5n3zebivJc00jI5+ixd51oH7/uFX8Lujtv4U/oh+GdzyF98Ft4qp05+DYrPln/m/zPygw/nwGg6PVa4bBsOwf+R/4fsr4Drg3LHxnCD9r7puPwmCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiC6Az/A7wlbzvM/tZvAAAAAElFTkSuQmCC" alt="gmail Logo">
                                    Register With GitHub</strong>
                            </a>
                        </div>
                        <div class="facebook-login btn-facebook col-md-6 offset-md-4">
                            <a href="{{ url('auth/facebook') }}" class="btn btn-facebook">
                                <strong>
                                    <img class="social-logo" src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTx5beFXvWOVbaP5gyvCcpziUlhgAxA82OACw&usqp=CAU" alt="gmail Logo">
                                    Register With Facebook</strong>
                            </a>
                        </div>
                    </div>

                    <p class="text-center already-registered">Already have an account? <a href="http://localhost:8000/login">Log In</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection