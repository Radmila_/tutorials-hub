$(document).ready(function() {

    var filteredDataResult = [];
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    let slug = window.location.pathname;

    $('#exampleModal').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('whatever') // Extract info from data-* attributes
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        modal.find('.modal-title-log-in').text('New Message ')
        modal.find('.modal-body input').val(recipient)
    })


    function subcategories() {
        $.post('/subcategories', slug, function(data) {
            data.forEach(element => {
                console.log(data);
                if ('/' + element.slug == slug) {
                    element.subcategories.forEach(sub => {
                        $('.subcategories').append(`
                        <div class="col-md-4">
                            <div class="logo w-100 rounded p-3 my-4 topic-grid-item js-topic-grid-item">
                                <a  href="courses/${sub.slug}" class="d-flex align-items-center">
                                    <img width="50px" height="50px" src="${sub.logo}">
                                    <h6 class="ml-3 js-topic">${sub.title}</h6>
                                </a>
                            </div>
                        </div>
                        `)
                    })
                } else if (slug == '/') {
                    element.subcategories.forEach(sub => {
                        $('.subcategories').append(`
                        <div class="col-md-4">
                            <div class="logo w-100 rounded p-3 my-4 topic-grid-item js-topic-grid-item">
                                <a href="courses/${sub.slug}" class="d-flex align-items-center">
                                    <img width="50px" height="50px" src="${sub.logo}">
                                    <h6 class="ml-3 js-topic">${sub.title}</h6>
                                </a>
                            </div>
                        </div>
                        `)
                    })
                }
            });
        })
    }
    subcategories();

    var test = slug.replace('/courses/', '');

    // function listFilters(data, table, column, isFilterVersion = false) {
    //     //HERE
    //     console.log(data);
    //     if (!data.length) {
    //         $.post('/listCourses', {
    //             'slug': test
    //         }, function(data) {
    //             setFilters(data, isFilterVersion, table, column);
    //         });
    //     } else {
    //         setFilters(fitleredData, isFilterVersion, table, column);
    //     }



    // };

    // function setFilters(data, isFilterVersion, table, column) {
    //     console.log("set fitlers")
    //     console.log(data);
    //     if (isFilterVersion) {
    //         filter = data.map(element => element[table][0][column]);
    //     } else {
    //         filter = data.map(element => element[table][column]);

    //     }
    //     console.log(filter);
    //     var counts = {};
    //     filter.forEach(function(x) {
    //         counts[x] = (counts[x] || 0) + 1;
    //     });

    //     let unique = [...new Set(filter)];
    //     unique.forEach(u => {
    //         $(`.${column}`).append(`<span class ="filters-names"><input type="checkbox" id="${u}-${table}-${column}" class="form-check-input" id=${u}><span>${u} (${counts[u]})</span></span><br>`);
    //     });

    // }

    // function countFilters() {
    //     $.post('/listCourses', slug, function(data) {
    //         // console.log(data);
    //         console.log(data.map(element => element.languages.language));

    //         let filters = data.map(element => element.languages.language);
    //         console.log(filters);

    //         var counts = {};
    //         filters.forEach(function(x) {
    //             counts[x] = (counts[x] || 0) + 1;

    //         });

    //         $(`.filters-number`).append(`English (${counts.English})`);
    //         $(`.filters-number`).append(counts.Deutsch);
    //         console.log("English: " + counts.English + "<br>");
    //         console.log("Deutsch: " + counts.Deutsch + "<br>");
    //         console.log("Français: " + counts.Français + "<br>");

    //     });
    // };

    // countFilters();




    // function listVersions(versions, version) {
    //     let slug = window.location.pathname
    //     $.post('/listCourses', slug, function(data) {
    //         // console.log(data);
    //         // console.log(vidovi);
    //         // console.log(data.map(element => element));
    //         console.log(data.map(element => element.versions[0].version));

    //         let filter = data.map(element => element.versions[0].version);
    //         // console.log(types);
    //         let unique = [...new Set(filter)];
    //         unique.forEach(u => {
    //             $(`.${version}`).append(`<a href="">-${u}</a><br>`);
    //         });
    //     });

    // };
    $('.nav-item').on('click', function() {
        $('.dropdown').toggleClass('show');
        $('.dropdown-menu').toggleClass('show');

    })

    // $('#clear-filters').on('click', function() {
    //     listCourses();
    // })

    function listCourses() {

        $.post('/listCourses', {
            'slug': test
        }, function(data) {
            // console.log(data);
            // currentlyDisplayedData = data;
            displayCourses(data.courses);
            // console.log(data.courses);
            appendTypes(data.finalType);
            appendMedium(data.finalMedium);
            appendVersion(data.finalVersion);
            appendSubLogo(data.subcategories);
            appendLevel(data.finalLevel);
            appendLanguage(data.finalLanguage);

        })

    };

    // function setVoting(element) {
    //     var voteClass = 'notvoted';

    //     element.coursesvotes.forEach(v => {
    //         console.log(AuthUser.id);
    //         if (v.pivot['user_id'] == AuthUser.id) {
    //             voteClass = "voted";
    //         }
    //     })
    //     return voteClass;
    // }

    listCourses();
    // listVersions('versions', 'version');
    // var data = []
    // listFilters(data, "types", "type");
    // listFilters(data, "versions", "version", true);
    // listFilters(data, "levels", "level");
    // listFilters(data, "languages", "language");
    // listFilters(data, "media", "medium");


    //filtriranje
    // $(document).on('change', '[type=checkbox]', function() {
    //     selectedFitler = true;
    //     var id = $(this).attr('id');
    //     let idData = id.split("-");
    //     let criteria = idData[0];
    //     // $(this).prop('checked', true);

    //     let categoryCriteria = idData[1];
    //     let column = idData[2];
    //     $.post('/listCourses', {
    //         'slug': test
    //     }, function(data) {
    //         if (filteredDataResult && filteredDataResult.length) {
    //             p = getFitleredData(result, criteria, categoryCriteria, column);
    //             result = p;
    //         } else {

    //             result = getFitleredData(data, criteria, categoryCriteria, column);
    //         }

    //         console.log("resultresultresultresult");
    //         console.log(result);
    //         // result.forEach(element => {
    //         //     filteredDataResult.push(element);

    //         // });
    //         filteredDataResult = result;
    //         console.log("====================");
    //         console.log(filteredDataResult);

    //         $('.courses').empty();
    //         filteredDataResult.forEach(element => {
    //             displayCourses(element, setVoting(element));

    //         });
    //         // listFilters(fitleredData, categoryCriteria, column, isFilterVersion = false)
    //         var className = `.${column}`
    //         console.log(className);
    //         console.log(column);

    //         clearFilters();

    //         listFilters(getFitleredData(data, criteria, categoryCriteria, column), "types", "type");
    //         listFilters(getFitleredData(data, criteria, categoryCriteria, column), "versions", "version", true);
    //         listFilters(getFitleredData(data, criteria, categoryCriteria, column), "levels", "level");
    //         listFilters(getFitleredData(data, criteria, categoryCriteria, column), "languages", "language");
    //         listFilters(getFitleredData(data, criteria, categoryCriteria, column), "media", "language");
    //     });

    // })

    // function getFitleredData(data, criteria, categoryCriteria, column) {
    //     if (criteria === "Version 1" || criteria === "Version 2") {
    //         fitleredData = data.filter(element => { return element[categoryCriteria][0][column] == criteria });
    //     } else {
    //         fitleredData = data.filter(element => { return element[categoryCriteria][column] == criteria });
    //     }
    //     return fitleredData;
    // }
    //filtriranje

    $('body').on('mouseover', function() {

        $('[data-toggle="tooltip"]').tooltip();
    })

    // function clearFilters() {
    //     $('.type').empty();
    //     $('.media').empty();
    //     $('.level').empty();
    //     $('.version').empty();
    //     $('.language').empty();
    // }

    function dashboard() {
        $.post('/dashboard', function(data) {
            // console.log(data);
            let classApproved;
            let status;
            let btnApprove;
            data.forEach(element => {
                // console.log(element.is_approved);
                if (element.is_approved == 0) {
                    status = "Unapproved";
                    btnApprove = 'Approve';
                    classApproved = '.unapproved-courses';
                } else if (element.is_approved == 1) {
                    status = "Approved";
                    btnApprove = 'Disapprove';
                    classApproved = '.approved-courses';
                }

                $(classApproved).append(`
                <tr>
                <td>${element.id}</td>
                <td><a href="${element.url}">${element.title}</a></td>
                <td>${element.types.type}</td>
                <td>${element.media.medium}</td>
                <td>${element.levels.level}</td>
                <td>${element.versions[0].version}</td>
                <td>${element.languages.language}</td>
                <td>${status}</td>
                <td><button data-id="${element.id}" class="btn btn-info ${btnApprove}">${btnApprove}</button>
                <button data-id="${element.id}" class="btn btn-danger delete">Delete course</button></td>
                </tr>
                `)
            });
        })
    };



    dashboard();

    $('body').on('click', '.votes', function(e) {
        e.preventDefault();
        var elementTest = this;
        // console.log('test')
        var voted = $(elementTest).attr('data-vote');
        var course_id = $(elementTest).attr('data-id');
        // console.log($(elementTest).find('.votes-number'))
        if (AuthUser == '') {
            $("#voteModal").modal("show");
        } else if (AuthUser.is_active == 0) {
            $("#confirmEmailModal").modal("show");
        } else {
            $(elementTest).find('.votes-number').empty();


            $.post('/vote/' + course_id, {
                'voted': voted,
                'course_id': course_id
            }, function(data) {
                // console.log(data.votes);
                if ($(elementTest).hasClass('colorYes')) {
                    $(elementTest).removeClass('colorYes');
                    $(elementTest).attr('data-vote', 'notvoted')
                } else {
                    $(elementTest).addClass('colorYes');
                    $(elementTest).attr('data-vote', 'voted')
                }
                // console.log(user1);
                // if ($(elementTest).attr('data-vote') == 'voted') {
                //     $(elementTest).attr('data-vote', 'notvoted')

                //         // $('.vote-widget').css('background-color', '#c1c1c1');
                // } else if ($(elementTest).attr('data-vote') == 'notvoted') {
                //     $(elementTest).attr('data-vote', 'voted')
                //         // $('.vote-widget').css('background-color', '#c1c1c1');

                // }
                $(elementTest).find('.votes-number').text(data['votes']);

            })
        }

    })

    function displayCourses(value) {
        value.forEach(element => {
            var voteClass = 'notvoted';
            var bgclass = '';
            element.coursesvotes.forEach(v => {
                console.log(AuthUser.id);
                if (v.pivot['user_id'] == AuthUser.id) {
                    voteClass = "voted";
                    bgclass = 'colorYes';
                }
            });
            nizaversions = [];
            element.versions.forEach(test => {
                    nizaversions.push(test.version);
                })
                // console.log(element);
                // if ('/courses/' + sub.slug == slug) {
            $('.courses-test').append(`
                    <div class="tut-list row">
                        <div class="col-md-2">
                        <div class="tut-vote">
                            <a href="" class="votes vote-widget js-tutorial-upvote ${bgclass}" data-vote="${voteClass}" data-id="${element.id}"> 
                            <span class="arrow"> <img class="arrow-img gray" src="https://hackr.io/assets/images/header-icons/caret-up.svg"> </span> 
                            <span class="count">
                                <p class="votes-number">${element.votes}</p>
                            </span>                        
                            <span class="tut-upvotes-text hidden">Upvote</span> </a> </div>
                        </div>
                        <div>
                            <a class="d-flex align-items-center title" href="${element.url}" target="_blank">
                                <span class="title-links">${element.title}</span>
                            </a><br>
                            <span class="tag-tooltip label-primary password" data-toggle="tooltip" data-placement="top" title="This is a${element.types['type'].toLowerCase()} tutorial">${element.types['type']}</span> 
                            <span class="tag-tooltip label-primary" data-toggle="tooltip" data-placement="top" title="This tutorial is in a ${element.media['medium'].toLowerCase()} format">${element.media['medium']}</span> 
                            <span class="tag-tooltip label-primary" data-toggle="tooltip" data-placement="top" title="This tutorial is suitable for ${element.levels['level'].toLowerCase()} level">${element.levels['level']}</span> 
                            <span class="tag-tooltip label-primary" data-toggle="tooltip" data-placement="top" title="Version">${nizaversions}</span>
                            <span class="tag-tooltip label-primary" data-toggle="tooltip" data-placement="top" title="Language">${element.languages['language']}</span><br>
                            <p class="submitedBy">Submited by: ${element.users.name}</p>
                        </div>
                    </div>
                `);
            //}
        })
    }

    function appendSubLogo(value) {
        $('.subcategory-title').html(`
            <div class="col-md-10 ">
                <div class="logo w-100 rounded p-3 my-4 row">
                    <img class="col-md-2" width="150px" height="70px" style="margin-left:-1rem!important" src="${value.logo}">
                    <div class=col-md-10>
                    <h3>${value.title} tutorials and courses.</h3>
                    <p>Learn ${value.title} online from the best ${value.title} tutorials
                        submitted & voted by the programming community.
                    </p>
                    </div>
                </div>
            </div>
        `)
    }

    function appendTypes(value) {
        value.forEach(filter => {
            $('.type-append').append(`
                <p>
                    <input type="checkbox" data-value="${filter.type}" value="${filter.id}" name="type_id" class="form-check-input filtration"> ${filter.type} (${filter.number})
                </p>
            `)
        })
    }

    function appendMedium(value) {
        value.forEach(filter => {
            $('.medium-append').append(`
                <p>
                    <input type="checkbox" data-value="${filter.medium}" value="${filter.id}" name="medium_id" class="form-check-input filtration"> ${filter.medium} (${filter.number})
                </p>
            `)
        })
    }

    function appendVersion(value) {
        value.forEach(filter => {
            $('.version-append').append(`
                <p>
                    <input type="checkbox" data-value="${filter.version}" value="${filter.id}" name="version_id" class="form-check-input filtration"> ${filter.version} (${filter.number})
                </p>
            `)
        })
    }

    function appendLevel(value) {
        // console.log(value);
        value.forEach(filter => {
            $('.level-append').append(`
                <p>
                    <input type="checkbox" data-value="${filter.level}" value="${filter.id}" name="level_id" class="form-check-input filtration"> ${filter.level} (${filter.number})
                </p>
            `)
        })
    }

    function appendLanguage(value) {
        // console.log(value);
        value.forEach(filter => {
            $('.language-append').append(`
                <p>
                    <input type="checkbox" data-value="${filter.language}" value="${filter.id}" name="language_id" class="form-check-input filtration"> ${filter.language} (${filter.number})
                </p>
            `)
        })
    }

    $('body').on('change', '.filtration', function(e) {
        e.preventDefault();
        var pathname = window.location.pathname;
        var slug = pathname.replace('/courses/', '');
        $('.courses-test').empty();
        var filters = {};
        $('.filtration:checked').each(function(i, e) {
            var filterName = $(this).attr('name');
            var filterValue = $(this).attr('value');
            filters[filterName] = filterValue;
        })
        filters['slug'] = slug;

        $.post('/filterCourses', filters, function(data) {
            console.log(data);
            displayCourses(data.courses);
        })
    })

    // function approveCourse(data) {
    //     $.post('/dashboard', function(data) {
    //         // console.log(data);
    //         $()
    //     })
    // }
    // approveCourse();

    $('body').on('click', '.Approve', function(e) {
        e.preventDefault();
        var course_id = $(this).attr('data-id');
        $('.approved-courses').empty();
        $('.unapproved-courses').empty();
        $.post('/approveCourse', {
            'id': course_id
        }, function(data) {
            dashboard();
        })
    })

    $('body').on('click', '.Disapprove', function(e) {
        e.preventDefault();
        var course_id = $(this).attr('data-id');
        $('.approved-courses').empty();
        $('.unapproved-courses').empty();
        $.post('/disapproveCourse', {
            'id': course_id
        }, function(data) {
            dashboard();
        })
    });

    $('body').on('click', '.delete', function(e) {
        e.preventDefault();
        var course_id = $(this).attr('data-id');
        $('.approved-courses').empty();
        $('.unapproved-courses').empty();
        $.post('/deleteCourse', {
            'id': course_id
        }, function(data) {
            dashboard();
        })
    });

    //check this
    $('.btn-submit-tutorial').on('click', function(e) {
        e.preventDefault();
        let title = $('#title').val()
        let url = $('#url').val()
        let type_id = $('.type').val()
        let medium_id = $('.medium').val()
        let level_id = $('.level').val()
        let language_id = $('.language').val()
        let subcategory_id = $('.subcategory').val()
        let version_id = $('.version').val()
            // console.log(version_id);

        // let user_id = $("#user_id").val()
        // console.log(user_id);

        $.post('/addCourse', {
            'title': title,
            'url': url,
            'type_id': type_id,
            'medium_id': medium_id,
            'level_id': level_id,
            'language_id': language_id,
            'subcategory_id': subcategory_id,
            'version_id': version_id,

            // 'user_id': user_id


        }, function(data) {
            console.log('success')
                // $('.modal-body').empty();
            $('#exampleModal').attr('data-backdrop', 'false');
            $('#submitMessageModal').attr('data-backdrop', 'false');
            // $('.modal-body').append(`
            //     <p class="modal-message">Your tutorial has been submited successfully. You will be able to see the tutorial once the admin has approved of it. Thank you for your contribution!</p>
            //     `)


        });
    });




    // var AuthUser = { Auth() - > check() ? Auth() - > user() : 'false'!! };
    // console.log(AuthUser);



    $('#search').on('keyup', function(e) {
        e.preventDefault();
        let search = $('#search').val();
        $.post('/search', {
            'search': search,
            'slug': slug
        }, function(data) {
            console.log(data)
            $('.subcategories').empty();
            data.forEach(sub => {
                $('.subcategories').append(`
                    <div class="col-md-4">
                        <div class="logo w-100 rounded p-3 my-4">
                            <a class="d-flex align-items-center" href="courses/${sub.slug}">
                                <img width="50px" height="50px" src="${sub.logo}">
                                <h6 class="ml-3">${sub.title}</h6>
                            </a>
                        </div>
                    </div>
                    `)
            });
        })
    })
})