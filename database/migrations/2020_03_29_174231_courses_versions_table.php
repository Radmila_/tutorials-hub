<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CoursesVersionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses_versions', function(Blueprint $table){
            $table->bigIncrements('id');
            $table->unsignedBigInteger('course_id')->onDelete('cascade'); //check this
            $table->unsignedBigInteger('version_id')->onDelete('cascade'); //check this
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses_versions');
    }
}
