<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VersionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('versions')
        ->insert([
            ['version' => 'Version 1'],
            ['version' => 'Version 2'],
        ]);
    }
}
