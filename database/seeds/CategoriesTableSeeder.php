<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')
            ->insert([
                ['title' => 'Programming', 'slug' => 'programming'],
                ['title' => 'Data Science', 'slug' => 'data-science'],
                ['title' => 'DevOps', 'slug' => 'devops'],
                ['title' => 'Design', 'slug' => 'design'],
            ]);
    }
}
