<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')
        ->insert([
            ['name' => 'John', 'email' => 'john@gmail', 'password' => bcrypt('123456789'), 'is_admin' => 1]
        ]);
    }
}
