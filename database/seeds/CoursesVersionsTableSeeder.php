<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CoursesVersionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('courses_versions')
        ->insert([
            ['course_id' => '1', 'version_id' => '1'],
            ['course_id' => '2', 'version_id' => '2'],
            ['course_id' => '3', 'version_id' => '1'],
            ['course_id' => '4', 'version_id' => '2'],
            ['course_id' => '5', 'version_id' => '2'],
            ['course_id' => '6', 'version_id' => '2'],
            ['course_id' => '7', 'version_id' => '2'],
            ['course_id' => '8', 'version_id' => '1'],
            ['course_id' => '9', 'version_id' => '2'],
            ['course_id' => '10', 'version_id' => '1'],
            ['course_id' => '11', 'version_id' => '2'],
            ['course_id' => '12', 'version_id' => '1'],
            ['course_id' => '13', 'version_id' => '2'],

        ]);
    }
}
