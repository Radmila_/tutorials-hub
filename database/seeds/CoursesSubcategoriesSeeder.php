<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CoursesSubcategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('courses_subcategories')
            ->insert([
                ['course_id' => '1', 'subcategory_id' => '1'],
                ['course_id' => '2', 'subcategory_id' => '1'],
                ['course_id' => '3', 'subcategory_id' => '2'],
                ['course_id' => '4', 'subcategory_id' => '4'],
                ['course_id' => '5', 'subcategory_id' => '7'],
                ['course_id' => '6', 'subcategory_id' => '57'],
                ['course_id' => '7', 'subcategory_id' => '58'],
                ['course_id' => '8', 'subcategory_id' => '65'],
                ['course_id' => '9', 'subcategory_id' => '66'],
                ['course_id' => '10', 'subcategory_id' => '79'],
                ['course_id' => '11', 'subcategory_id' => '89'],
                ['course_id' => '12', 'subcategory_id' => '1'],
                ['course_id' => '13', 'subcategory_id' => '1'],

            ]);
    }
}
