<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CoursesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('courses')
            ->insert([
                [
                    'title' => 'Complete Python Bootcamp',
                    'url' => 'https://www.udemy.com/course/complete-python-bootcamp/?ranMID=39197&ranEAID=jU79Zysihs4&ranSiteID=jU79Zysihs4-elrWLp8rowA6wuBRIOxuBw&LSNPUBID=jU79Zysihs4',
                    'votes' => '0',
                    'is_approved' => '0',
                    'type_id' => '2',
                    'level_id' => '1',
                    'medium_id' => '1',
                    'language_id' => '1',
                    'user_id' => '1',
                ],

                [
                    'title' => 'Python for Everybody Specialization',
                    'url' => 'https://www.coursera.org/specializations/python?ranMID=40328&ranEAID=jU79Zysihs4&ranSiteID=jU79Zysihs4-wK0ZkMWs54.lbkDcAqLIxQ&siteID=jU79Zysihs4-wK0ZkMWs54.lbkDcAqLIxQ&utm_content=10&utm_medium=partners&utm_source=linkshare&utm_campaign=jU79Zysihs4',
                    'votes' => '0',
                    'is_approved' => '0',
                    'type_id' => '1',
                    'level_id' => '1',
                    'medium_id' => '1',
                    'language_id' => '1',
                    'user_id' => '1',
                ],

                [
                    'title' => 'Eloquent JavaScript',
                    'url' => 'https://eloquentjavascript.net/?ref=hackr.io',
                    'votes' => '0',
                    'is_approved' => '0',
                    'type_id' => '1',
                    'level_id' => '1',
                    'medium_id' => '2',
                    'language_id' => '1',
                    'user_id' => '1',
                ],

                [
                    'title' => 'Head First Java',
                    'url' => 'https://www.amazon.it/dp/0596009208?tag=hackr069-21',
                    'votes' => '0',
                    'is_approved' => '0',
                    'type_id' => '1',
                    'level_id' => '1',
                    'medium_id' => '2',
                    'language_id' => '1',
                    'user_id' => '1',
                ],

                [
                    'title' => 'AwesomeReact',
                    'url' => 'https://github.com/enaqx/awesome-react?ref=hackr.io',
                    'votes' => '0',
                    'is_approved' => '0',
                    'type_id' => '1',
                    'level_id' => '1',
                    'medium_id' => '2',
                    'language_id' => '1',
                    'user_id' => '1',
                ],

                [
                    'title' => 'Artificial Intelligence A-Z™: Learn How To Build An AI',
                    'url' => 'https://click.linksynergy.com/deeplink?id=jU79Zysihs4&mid=39197&murl=https://www.udemy.com/artificial-intelligence-az/',
                    'votes' => '0',
                    'is_approved' => '0',
                    'type_id' => '2',
                    'level_id' => '1',
                    'medium_id' => '2',
                    'language_id' => '1',
                    'user_id' => '1',
                ],

                [
                    'title' => 'Machine Learning A-Z: Hands-On Python & R In Data Science',
                    'url' => 'https://www.udemy.com/course/machinelearning/?ranMID=39197&ranEAID=jU79Zysihs4&ranSiteID=jU79Zysihs4-B8dq5jmrLz.huPOhAZVynQ&LSNPUBID=jU79Zysihs4',
                    'votes' => '0',
                    'is_approved' => '0',
                    'type_id' => '2',
                    'level_id' => '1',
                    'medium_id' => '2',
                    'language_id' => '1',
                    'user_id' => '1',
                ],

                [
                    'title' => 'Linux Command Line Basics',
                    'url' => 'https://click.linksynergy.com/deeplink?id=jU79Zysihs4&mid=39197&murl=https://www.udemy.com/linux-command-line-volume1/',
                    'votes' => '0',
                    'is_approved' => '0',
                    'type_id' => '2',
                    'level_id' => '1',
                    'medium_id' => '2',
                    'language_id' => '1',
                    'user_id' => '1',
                ],

                [
                    'title' => 'Amazon Web Services Fundamentals for System Administrators (pluralsight.com)',
                    'url' => 'https://www.pluralsight.com/courses/aws-system-admin-fundamentals?clickid=zRkQDXxWcxyOWKYwUx0Mo3ERUkixtoQNBQd4yM0&irgwc=1&mpid=1419154&utm_source=impactradius&utm_medium=digital_affiliate&utm_campaign=1419154&aid=7010a000001xAKZAA2',
                    'votes' => '0',
                    'is_approved' => '0',
                    'type_id' => '2',
                    'level_id' => '1',
                    'medium_id' => '2',
                    'language_id' => '1',
                    'user_id' => '1',
                ],

                [
                    'title' => 'Ultimate Photoshop Training: From Beginner to Pro (udemy.com)',
                    'url' => 'https://www.udemy.com/course/ultimate-photoshop-training-from-beginner-to-pro/?LSNPUBID=jU79Zysihs4&ranEAID=jU79Zysihs4&ranMID=39197&ranSiteID=jU79Zysihs4-FXk3gE4jpTfg340HoZgl_w',
                    'votes' => '0',
                    'is_approved' => '0',
                    'type_id' => '2',
                    'level_id' => '1',
                    'medium_id' => '2',
                    'language_id' => '1',
                    'user_id' => '1',
                ],

                [
                    'title' => 'User Interface Design (javatpoint.com)',
                    'url' => 'https://www.javatpoint.com/software-engineering-user-interface-design?ref=hackr.io',
                    'votes' => '0',
                    'is_approved' => '0',
                    'type_id' => '1',
                    'level_id' => '1',
                    'medium_id' => '1',
                    'language_id' => '1',
                    'user_id' => '1',
                ],

                [
                    'title' => 'Learning Python with PyCharm (linkedin.com)',
                    'url' => 'https://www.linkedin.com/learning/learning-python-with-pycharm?src=aff-lilpar&veh=aff_src.aff-lilpar_c.partners_pkw.1419154_plc.Hackr.io_pcrid.449670_learning&trk=aff_src.aff-lilpar_c.partners_pkw.1419154_plc.Hackr.io_pcrid.449670_learning&clickid=zRkQDXxWcxyOWKYwUx0Mo3ERUkixKv2lBQd9S80&irgwc=1',
                    'votes' => '0',
                    'is_approved' => '0',
                    'type_id' => '2',
                    'level_id' => '2',
                    'medium_id' => '1',
                    'language_id' => '2',
                    'user_id' => '1',
                ],

                [
                    'title' => 'Learn Python the Hard Way (amazon)',
                    'url' => 'https://geni.us/XNd0BY?ref=hackr.io',
                    'votes' => '0',
                    'is_approved' => '0',
                    'type_id' => '2',
                    'level_id' => '1',
                    'medium_id' => '2',
                    'language_id' => '1',
                    'user_id' => '1',
                ],
            ]);
    }
}
