<?php

namespace App\Http\Controllers;

use App\Category;
use App\Course;
use App\Subcategory;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function categories()
    {if(empty(\Request('slug'))) {
        $subcategories = Category::with('subcategories')->get();
    } else if(\Request('slug') == 'dashboard'){
        return view('dashboard');
    } else if(strpos(\Request('slug'), 'courses') !== false) {
        $courses = Course::all();
        return redirect()->route('courses', ['courses' => $courses]);
    }
    else {
        $subcategories = Category::with('subcategories')->where('slug', \Request('slug'))->first();
    }

    // dd($subcategories);
        return view('index', ['subcategories' => $subcategories]);
    }

    public function subcategories(){
        if(empty(\Request('slug'))) {
            $subcategories = Category::with('subcategories')->get();
        } else {
            $subcategories = Category::with('subcategories')->where('slug', \Request('slug'))->first();
        }
        return response()->json($subcategories);
    }

    // public function search(Request $request){
    //     $subcategories = Subcategory::where('title', 'LIKE', '%'.$request->get('search').'%')->get();
        
    //     return response()->json($subcategories);
    // }

    public function search(Request $request) {
        if($request->slug == "/") {
            $subcategories = Subcategory::where('title', 'LIKE', '%'.$request->get('search').'%')->get();
        } else {
            $category = Category::where('slug', trim($request->slug, "/"))->first()->id;
            $subcategories = Subcategory::with('categories')->where('category_id', $category)
            ->where('title', 'LIKE', '%'.$request->get('search').'%')
            ->get();
        }
        return response()->json($subcategories);
    }
}
