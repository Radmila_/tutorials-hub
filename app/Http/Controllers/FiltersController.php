<?php

namespace App\Http\Controllers;

use App\Course;
use Illuminate\Http\Request;

class FiltersController extends Controller
{
    public function indexFilters(Request $request){
        return Course::filter($request)->get();
    }
}
