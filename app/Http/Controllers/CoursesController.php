<?php

namespace App\Http\Controllers;

use App\Category;
use App\Course;
use App\Subcategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CoursesController extends Controller
{
    public function courses()
    {
        // $courses = Course::with('subcategories', 'versions')->get();
        // $courses = Course::all();
        // dd($courses);

        $courses = Course::all();

        // $subcategories
        // dd($subcategories);
        return view('courses', ['courses' => $courses]);
    }

    public function listCourses(Request $request)
    {

        $subcategories = Subcategory::with('courses')->where('slug', $request->slug)->first();
        // $subcategoriesCourses = DB::table('courses_subcategories')->where('subcategory_id', $subcategories->id)->get();
        $coursesTest = $subcategories->courses;
        $courses = [];
        foreach ($coursesTest as $course) {
            if($course->is_approved == 1){
                $courses[] = $course;
            }
        }

        $arrfortype = [];
        foreach ($courses as $type) {
            $arrfortype[] = $type->types['id'];
        }
        $countedType = array_count_values($arrfortype);
        $finalType = [];
        foreach ($countedType as $t => $v) {
            $type = \App\Type::where('id', $t)->first();
            $finalType[] = ['id' => $t, 'type' => $type->type, 'number' => $v];
        }
        array_multisort(array_column($finalType, 'id'), SORT_ASC, $finalType);

        $arrformedium = [];
        foreach ($courses as $medium) {
            $arrformedium[] = $medium->media['id'];
        }
        $countedMedium = array_count_values($arrformedium);
        $finalMedium = [];
        foreach ($countedMedium as $t => $v) {
            $medium = \App\Medium::where('id', $t)->first();
            $finalMedium[] = ['id' => $t, 'medium' => $medium->medium, 'number' => $v];
        }
        array_multisort(array_column($finalMedium, 'id'), SORT_ASC, $finalMedium);

        $arrforlevel = [];
        foreach ($courses as $level) {
            $arrforlevel[] = $level->levels['id'];
        }
        $countedLevel = array_count_values($arrforlevel);
        $finalLevel = [];
        foreach ($countedLevel as $t => $v) {
            $level = \App\Level::where('id', $t)->first();
            $finalLevel[] = ['id' => $t, 'level' => $level->level, 'number' => $v];
        }
        array_multisort(array_column($finalLevel, 'id'), SORT_ASC, $finalLevel);

        $arrforlanguage = [];
        foreach ($courses as $language) {
            $arrforlanguage[] = $language->languages['id'];
        }
        $countedLanguage = array_count_values($arrforlanguage);
        $finalLanguage = [];
        foreach ($countedLanguage as $t => $v) {
            $language = \App\Language::where('id', $t)->first();
            $finalLanguage[] = ['id' => $t, 'language' => $language->language, 'number' => $v];
        }
        array_multisort(array_column($finalLanguage, 'id'), SORT_ASC, $finalLanguage);

        $arrforversion = [];
        foreach ($courses as $forversion) {
            foreach ($forversion->versions as $version) {
                $arrforversion[] = $version->id;
            }
        }
        $countedVersion = array_count_values($arrforversion);
        $finalVersion = [];
        foreach ($countedVersion as $t => $v) {
            $version = \App\Version::where('id', $t)->first();
            $finalVersion[] = ['id' => $t, 'version' => $version->version, 'number' => $v];
        }
        array_multisort(array_column($finalVersion, 'id'), SORT_ASC, $finalVersion);

        return response()->json(['subcategories' => $subcategories, 'courses' => $courses, 'finalVersion' => $finalVersion, 'finalType' => $finalType, 'finalMedium' => $finalMedium, 'finalLevel' => $finalLevel, 'finalLanguage' => $finalLanguage]);
    }

    public function filterCourses(Request $request)
    {
        $subcategories = Subcategory::with('courses')->where('slug', $request->slug)->first();
        // $subcategoriesCourses = DB::table('courses_subcategories')->where('subcategory_id', $subcategories->id)->get();
        $coursesTest = $subcategories->courses()->filter($request)->get();
        $courses = [];
        foreach ($coursesTest as $course) {
            if($course->is_approved == 1){
                $courses[] = $course;
            }
        }

        $arrfortype = [];
        foreach ($courses as $type) {
            $arrfortype[] = $type->types['id'];
        }
        $countedType = array_count_values($arrfortype);
        $finalType = [];
        foreach ($countedType as $t => $v) {
            $type = \App\Type::where('id', $t)->first();
            $finalType[] = ['id' => $t, 'type' => $type->type, 'number' => $v];
        }
        array_multisort(array_column($finalType, 'id'), SORT_ASC, $finalType);

        $arrformedium = [];
        foreach ($courses as $medium) {
            $arrformedium[] = $medium->media['id'];
        }
        $countedMedium = array_count_values($arrformedium);
        $finalMedium = [];
        foreach ($countedMedium as $t => $v) {
            $medium = \App\Medium::where('id', $t)->first();
            $finalMedium[] = ['id' => $t, 'medium' => $medium->medium, 'number' => $v];
        }
        array_multisort(array_column($finalMedium, 'id'), SORT_ASC, $finalMedium);

        $arrforlevel = [];
        foreach ($courses as $level) {
            $arrforlevel[] = $level->levels['id'];
        }
        $countedLevel = array_count_values($arrforlevel);
        $finalLevel = [];
        foreach ($countedLevel as $t => $v) {
            $level = \App\Level::where('id', $t)->first();
            $finalLevel[] = ['id' => $t, 'level' => $level->level, 'number' => $v];
        }
        array_multisort(array_column($finalLevel, 'id'), SORT_ASC, $finalLevel);

        $arrforlanguage = [];
        foreach ($courses as $language) {
            $arrforlanguage[] = $language->languages['id'];
        }
        $countedLanguage = array_count_values($arrforlanguage);
        $finalLanguage = [];
        foreach ($countedLanguage as $t => $v) {
            $language = \App\Language::where('id', $t)->first();
            $finalLanguage[] = ['id' => $t, 'language' => $language->language, 'number' => $v];
        }
        array_multisort(array_column($finalLanguage, 'id'), SORT_ASC, $finalLanguage);

        $arrforversion = [];
        foreach ($courses as $forversion) {
            foreach ($forversion->versions as $version) {
                $arrforversion[] = $version->id;
            }
        }
        $countedVersion = array_count_values($arrforversion);
        $finalVersion = [];
        foreach ($countedVersion as $t => $v) {
            $version = \App\Version::where('id', $t)->first();
            $finalVersion[] = ['id' => $t, 'version' => $version->version, 'number' => $v];
        }
        array_multisort(array_column($finalVersion, 'id'), SORT_ASC, $finalVersion);

        return response()->json(['subcategories' => $subcategories, 'courses' => $courses, 'finalVersion' => $finalVersion, 'finalType' => $finalType, 'finalMedium' => $finalMedium, 'finalLevel' => $finalLevel, 'finalLanguages' => $finalLanguage]);
    }

    public function vote($id, Request $request)
    {
        //  if(\Auth::user()){
        if ($request->voted == 'notvoted') {
            Course::where('id', $id)->increment('votes', 1);
            $course = Course::where('id', $id)->first();
            $course->coursesvotes()->attach(\Auth::user()->id);
        } else {
            Course::where('id', $id)->decrement('votes', 1);
            $course = Course::where('id', $id)->first();
            $course->coursesvotes()->detach(\Auth::user()->id);
        }

        return response()->json($course);
        // }
    }

    public function addCourse(Request $request)
    {
        $course = new Course();

        $course->title = $request->get('title');
        $course->url = $request->get('url');
        // $course->subcategory = $request->get('subcategory_id');
        $course->type_id = $request->get('type_id');
        $course->medium_id = $request->get('medium_id');
        $course->level_id = $request->get('level_id');
        $course->language_id = $request->get('language_id');
        $course->votes = 0;
        $course->user_id = \Auth::user()->id;
        $course->save();

        $course->subcategories()->attach($request->get('subcategory_id'));
        $course->versions()->attach($request->get('version_id'));


        return redirect()->route('index');
    }
}
