<?php

namespace App\Http\Controllers;

use App\Course;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function dashboard(){
        $courses = Course::with('subcategories', 'versions', 'users', 'types', 'media', 'levels', 'versions', 'languages')->get();
       
        return response()->json($courses);
    }

    public function approveCourse(Request $request){
        Course::where('id', $request->id)->update([
            'is_approved' => 1
        ]);

        $courses = Course::with('subcategories', 'versions', 'users', 'types', 'media', 'levels', 'versions', 'languages')->get();
        return response()->json($courses);
    }

    public function disapproveCourse(Request $request){
        Course::where('id', $request->id)->update([
            'is_approved' => 0
        ]);

        $courses = Course::with('subcategories', 'versions', 'users', 'types', 'media', 'levels', 'versions', 'languages')->get();
        return response()->json($courses);
    }

    public function deleteCourse(Request $request){
        $course = Course::find($request->id);
        $course->subcategories()->detach();
        $course->versions()->detach();
        $course->coursesvotes()->detach();
        
        Course::destroy($request->id);
       
        $courses = Course::with('subcategories', 'versions', 'users', 'types', 'media', 'levels', 'versions', 'languages')->get();
        return response()->json($courses);
    }
}
