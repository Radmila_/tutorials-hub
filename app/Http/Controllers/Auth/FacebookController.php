<?php

namespace App\Http\Controllers\Auth;


use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use Exception;
// use Auth;

class FacebookController extends Controller
{

    protected $redirectTo = '/';


    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public function redirectToGithub()
    {
        return Socialite::driver('github')->redirect();
    }

    



    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function handleFacebookCallback()
    {
        $facebookUser = Socialite::driver('facebook')->user();

        $user = User::firstOrCreate(
            [
                'email' => $facebookUser->email,
            ],
            [
                'name' => $facebookUser->name,
            ],
            [
                'facebook_id' => $facebookUser->id,
            ]
        );

        auth()->login($user);

        if($user->is_active == 0){
            $user->notify(new \App\Notifications\Users\ActivateEmailNotification($user));
        }
        // event(new RegisteredUser($user));
        return redirect()->route('index');
    }

    public function handleGoogleCallback()
    {
        $googleUser = Socialite::driver('google')->user();

        $user = User::firstOrCreate(
            [
                'email' => $googleUser->email,
            ],
            [
                'name' => $googleUser->name,
            ],
            [
                'facebook_id' => $googleUser->id,
            ]
        );

        auth()->login($user);

        if($user->is_active == 0){
            $user->notify(new \App\Notifications\Users\ActivateEmailNotification($user));
        }
        // event(new RegisteredUser($user));
        return redirect()->route('index');

    }

    public function handleGithubCallback()
    {
        $githubUser = Socialite::driver('github')->user();

        $user = User::firstOrCreate(
            [
                'email' => $githubUser->email,
            ],
            [
                'name' => $githubUser->nickname,
            ],
            [
                'facebook_id' => $githubUser->id,
            ]
        );

        auth()->login($user);

        if($user->is_active == 0){
            $user->notify(new \App\Notifications\Users\ActivateEmailNotification($user));
        }
        // event(new RegisteredUser($user));
        return redirect()->route('index');
    }

   
    // try {
    //     $user = Socialite::driver('facebook')->user();
    //     $create['name'] = $user->getName();
    //     $create['email'] = $user->getEmail();
    //     $create['facebook_id'] = $user->getId();


    //     $userModel = new User();
    //     $createdUser = $userModel->addNew($create);
    //     Auth::loginUsingId($createdUser->id);


    //     return redirect()->route('home');


    // } catch (Exception $e) {


    //     return redirect('auth/facebook');


    // }


        // }
    }
