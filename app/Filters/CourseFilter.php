<?php


namespace App\Filters;

use App\Filters\AbstractFilter;

class CourseFilter extends AbstractFilter
{
    protected $filters = [
        'type_id' => TypeFilter::class, 
        'medium_id' => MediumFilter::class, 
        'version_id' => VersionFilter::class, 
        'level_id' => LevelFilter::class, 
        'language_id' => LanguageFilter::class, 

    ];
}