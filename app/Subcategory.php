<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    public function categories(){
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function courses(){
        return $this->belongsToMany(Course::class, 'courses_subcategories');
    }
}
