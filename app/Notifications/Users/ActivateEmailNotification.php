<?php

namespace App\Notifications\Users;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\URL;

class ActivateEmailNotification extends Notification
{
    use Queueable;

    public $user;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        // dd($notifiable);
        $url = URL::signedRoute('activate-email', ['user' => $this->user]);
      
        return (new MailMessage)
                    ->subject('Confirm your email address')
                    ->line('These are your credentials:')
                    ->line('Name: '. $this->user->name)
                    ->line('Email: ' . $this->user->email)
                    ->line('Please, click on the following link to confirm your email and activate your account')
                    ->action('Confirm Email', $url)
                    ->line('Thank you for using our application!');
                    // ->view('emails.sendEmail', ['notifiable' => $notifiable]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
