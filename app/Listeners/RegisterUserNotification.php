<?php

namespace App\Listeners;

use App\Events\RegisteredUser;
use App\Mail\SendMail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;

class RegisterUserNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */

    // public function handle(RegisteredUser $event)
    // {
    //     $data[] = $event->user;
    //     $link = 'http://127.0.0.1:8000/verifyEmail/' . $event->user->email;
    //     $data['link'] = $link;
    //     $email = $event->user->email;
    //     \Mail::send( 'emails.sendEmail', $data, function ($message) use ($email) {
    //         $message->from ( 'anamarijanestorovska@gmail.com', 'Just Laravel' );
    //         $message->to ( $email )->subject ( 'Challenge 26 Email' );
    //     } );
    // }

    public function handle(RegisteredUser $event)
    {
        $data[] = $event->user;
        $email = $event->user->email;
        $link = 'http://localhost:8000/activateEmail/'. $email;
        $data['link'] = $link;
        // $hash = $event->user->verification_hash;
        // $url = URL::temporarySignedRoute('activate-email',  ['user' => $this->user->id]);
        // $link = URL::temporarySignedRoute('activate-email', now()->addMinutes(30), ['user' => $event->user->id]);
        // $link = 'http://localhost:8000/activate/'.$email.'/'.$hash;
        // $data = ['event' => $event];
        Mail::send( 'emails.sendEmail', $data, function ($message) use ($email) {
            $message->from('laravelevents@gmail.com', 'Just Laravel');
            $message->to($email)->subject('Just Laravel demo email using Gmail');

        });
    }
}
