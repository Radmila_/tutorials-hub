<?php

namespace App;

use App\Filters\CourseFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{

    protected $with = ['subcategories', 'users', 'types', 'media', 'levels', 'versions', 'languages', 'coursesvotes'];

    public function languages(){
        return $this->belongsTo(Language::class, 'language_id');
    }

    public function levels(){
        return $this->belongsTo(Level::class, 'level_id');
    }

    public function media(){
        return $this->belongsTo(Medium::class, 'medium_id');
    }

    public function types(){
        return $this->belongsTo(Type::class, 'type_id');
    }

    public function users(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function versions(){
        return $this->belongsToMany(Version::class, 'courses_versions');
    }

    public function subcategories(){
        return $this->belongsToMany(Subcategory::class, 'courses_subcategories');
    }

    public function coursesvotes(){
        return $this->belongsToMany(User::class, 'courses_users');
    }

    public function scopeFilter(Builder $builder, $request)
    {
        return (new CourseFilter($request))->filter($builder);
    }
}
